/*jshint node: true, esnext: true */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var competitionSchema = new Schema({
    name: {
      type: String,
      required: true
    },
    transmision: {
      type: Boolean,
      required: true,
      default: false
    },
    amount_of_referees: {
      type: Number,
      required: true
    },
    range_of_rates: {
      type: Number,
      required: true
    },
    type_of_rates: {
      type: String,
      required: true
    },
    startlist:  [{ type: mongoose.Schema.Types.ObjectId, ref: 'Startlist' }],
    categories: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Category' }],
    referees: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
});

module.exports = mongoose.model("Competition", competitionSchema);

