/*jshint node: true, esnext: true */
var mongoose = require('mongoose');
var relationship = require('mongoose-relationship');
var Schema = mongoose.Schema;


var rateSchema = new Schema({
    type: {
      type: Number,
      required: true,
      default:0
    },
    head_and_neck: {
      type: Number,
      required: true,
      default:0
    },
    log: {
      type: Number,
      required: true,
      default:0
    },
    legs: { 
        type: Number,
        required: true,
      default:0
    },
    motion: {
        type: Number,
        required: true,
        default:0
    },
    overall: {
        type: Number,
        default:0
    },
    startlist:  [{ type: mongoose.Schema.Types.ObjectId, ref: 'Startlist', childPath:"rate" }],
    referee:  [{ type: mongoose.Schema.Types.ObjectId, ref: 'User', childPath:"rate" }]
});

rateSchema.plugin(relationship, { relationshipPathName:'startlist' });
rateSchema.plugin(relationship, { relationshipPathName:'referee' });
module.exports = mongoose.model("Rate", rateSchema);