/*jshint node: true */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var relationship = require('mongoose-relationship');

var userSchema = new Schema({

    firstName: {
      type: String,
      //required: true
    },
    lastName: {
      type: String,
      //required: true
    },
    email: String,
    username: { 
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    type: {
      type: String,
      required: true
    },
    _startlist: { type: mongoose.Schema.Types.ObjectId, ref: 'Startlist' },
    competition: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Competition', childPath:"referees" }],
    rate:  [{ type: mongoose.Schema.Types.ObjectId, ref: 'Rate' }],
    category: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Category', childPath:"referees" }]
    
});
userSchema.plugin(relationship, { relationshipPathName:'competition' });
module.exports = mongoose.model("User", userSchema);