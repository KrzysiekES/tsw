/*jshint node: true, esnext: true */
var mongoose = require('mongoose');
var relationship = require('mongoose-relationship');
var Schema = mongoose.Schema;


var startlistSchema = new Schema({

    number: {
      type: Number,
      required: true
    },
    gender: {
      type: String,
      required: true
    },
    overall: {
      type: Number,
      default: 0
    },
   horse: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Horse'}],
   category:  [{ type: mongoose.Schema.Types.ObjectId, ref: 'Category' }],
   rate:  [{ type: mongoose.Schema.Types.ObjectId, ref: 'Rate' }],
   competition:  [{ type: mongoose.Schema.Types.ObjectId, ref: 'Competition', childPath:"startlist" }]

});

startlistSchema.plugin(relationship, { relationshipPathName:'competition' });

module.exports = mongoose.model("Startlist", startlistSchema);