/*jshint node: true, esnext: true */
var mongoose = require('mongoose');
var relationship = require('mongoose-relationship');
var Schema = mongoose.Schema;

var horseSchema = new Schema({
    name: {
      type: String,
      required: true
    },
    gender: {
      type: String,
      required: true
    },
    bornDate: {
      type: String,
      required: true
    },
    owner: {
      type: String,
      required: true
    },
    _startlist: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Startlist', childPath:"horse" }]
});

horseSchema.plugin(relationship, { relationshipPathName:'_startlist' });
module.exports = mongoose.model("Horse", horseSchema);