/*jshint node: true, esnext: true */
var mongoose = require('mongoose');
var relationship = require('mongoose-relationship');

var Schema = mongoose.Schema;


var categorySchema = new Schema({
    name: {
      type: String,
      required: true
    },
      transmision: {
      type: Boolean,
      required: true,
      default: false
    },
    _referees:  [{ type: mongoose.Schema.Types.ObjectId, ref: 'User', childPath:"category" }],
    _startlist:  [{ type: mongoose.Schema.Types.ObjectId, ref: 'Startlist', childPath:"category" }],
    _competition: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Competition', childPath:"categories" }]
});

categorySchema.plugin(relationship, { relationshipPathName:'_competition' });
categorySchema.plugin(relationship, { relationshipPathName:'_referees' });
categorySchema.plugin(relationship, { relationshipPathName:'_startlist' });
module.exports = mongoose.model("Category", categorySchema);
