/*jshint node: true, esnext: true, devel: true  */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//Przekierowanie do index.ejs i sprawdzenie czy jest zalogowany
exports.index = function (req, res) {
    res.render('index', {
        isAuthenticated: req.isAuthenticated(),
        user: req.user
    });
};

exports.live = function (req, res) {
    res.render('live', {
        isAuthenticated: req.isAuthenticated(),
        user: req.user
    });
};

exports.adminTransmisions = function (req, res) {
    res.render('adminTransmisions', {
        isAuthenticated: req.isAuthenticated(),
        user: req.user
    });
};

exports.competitionlist = function (req, res) {
    res.render('competitionlist', {
        isAuthenticated: req.isAuthenticated(),
        user: req.user
    });
};

exports.refereePanel = function (req, res) {
  
    res.render('refereePanel', {
        isAuthenticated: req.isAuthenticated(),
        user: req.user
    });
};

exports.showSingleLiveCategory = function(req, res){
    var Category = mongoose.model('Category');
  
    Category.find({_id: req.params.id, transmision: true }).populate('_competition').exec(function (err, category) {
      
        if (err) {
            console.log('Wystąpił błąd' + err);
        }else {
            res.send(category);
        }
    }); 
};

exports.refereePanelFindLive = function(req, res){
  
    var myid = req.user;
    var myCompetitions;
    var myCategories;
  
    var Competition = mongoose.model('Competition');
    var Category = mongoose.model('Category');
    
    Competition.find({referees: myid.id, transmision: true }).populate('referees').exec(function (err, competition) {
        if (err) {
            console.log('Wystąpił błąd' + err);
        }else {
            myCompetitions = competition;
            cb(myid.id);
        }
    });
    
    var cb = function(id){
        Category.find({_referees: id, transmision: true}).populate('referees').exec(function (err, category) {
            if (err) {
                console.log('Wystąpił błąd' + err);
            }else {
               myCategories = category;
              
               var data = {
                  refereeLiveCompetitions: myCompetitions,
                  refereeLiveCategories: myCategories,
                  referee: myid
               };
               res.send(data);
            }
        });
    };
};

exports.refereePanelFind = function(req, res){

    var myid = req.user;
    var myCompetitions;
    var myCategories;
  
    var Competition = mongoose.model('Competition');
    var Category = mongoose.model('Category');
    
    Competition.find({referees: myid.id }).populate('referees').exec(function (err, competition) {
      
        if (err) {
            console.log('Wystąpił błąd' + err);
        }else {
            myCompetitions = competition;
            cb(myid.id);
        }

    });
    
    var cb = function(id){
        Category.find({_referees: id}).populate('referees').exec(function (err, category) {
            if (err) {
                console.log('Wystąpił błąd' + err);
            }else {
                myCategories = category;

                var data = {
                    refereeCompetitions: myCompetitions,
                    refereeCategories: myCategories
                };

                  console.log(data);
                  res.send(data);
            }
        });
    };
};

exports.register = function( req, res) {
    res.render('register', {
        isAuthenticated: req.isAuthenticated(),
        user: req.user
    });
};

exports.registerNew = function(req, res){
  var passwordHash = require('password-hash');
  var User = mongoose.model('User');
  var user = new User();
  var status = false;
  
  user.email = req.body.email;
  user.username = req.body.username;
  user.password = passwordHash.generate(req.body.password);
  user.type = "admin";
  
  user.save(function(err, user_Saved){
      if(err){
          console.log('Nie zapisano');
          status = false;
      }else{
          console.log('Zapisano:' + user.username +  ' w bazie danych');
          status = true;
      }
    
      var data = {
          status: status
      }; 
      res.send(data);
  });
};

exports.redirectLogin = function (req, res) {
    res.render('login', {
        isAuthenticated: req.isAuthenticated(),
        user: req.user,
        referee: req.referee
    });
};

exports.logout = function (req, res) {
    req.logout();
    res.redirect('/');
};

exports.users =  function (req, res) {
    User.find({}, function (err, data) {
        if (err) {
            console.log("nie ma");
            res.code(500);
            
        } else {
            res.json(data);
        }
    });
};

exports.addReferee = function(req, res) {
    var passwordHash = require('password-hash');
    var Referee = mongoose.model('User');
    var referee = new Referee();


    referee.firstName = req.body.firstName;
    referee.lastName = req.body.lastName;
    referee.email = req.body.email;
    referee.username = req.body.username;
    referee.password = passwordHash.generate(req.body.password);
    referee.type = 'referee';

    var status = false;
  
    referee.save(function(err, referee_Saved){
        if(err){
            console.log("Nie zapisane" + err);
            status = false;
        }else{
            status = true;
            console.log("Zapisano: " + referee.firstName + " " + referee.lastName );
            console.log("status" + status );
        }
    
        var data = {
            status: status
        }; 
        res.send(data);
  });
};

exports.referee = function(req, res) {
    res.render('referee', {
        isAuthenticated: req.isAuthenticated(),
        user: req.user
    });
};

exports.showReferees = function(req, res){
    var Referee = mongoose.model('User');
    //console.log("czy jest zaogowany: " + req.isAuthenticated() + "user" + req.user);
    Referee.find({type: 'referee'}, function (err, referee) {
        console.log("Znaleziono: " + referee);
        res.send(referee);
    });
};

exports.deleteReferee = function(req, res){
    console.log(req.params.id);
    var Referee = mongoose.model('User');
    var status = false;
  
    Referee.remove({_id: req.params.id},function(err){
        if (!err) {
            status = true;
            console.log("Usunięto");
        }
        else {
            status=false;
            console.log("Nie usunięto");
        }
    });
  
    var data = {
        status: status
    }; 
    
    res.send(data);
};

exports.horses = function(req, res){
  res.render('horses', {
        isAuthenticated: req.isAuthenticated(),
        user: req.user
    });
};

exports.addHorse = function(req, res) {
  var Horse = mongoose.model('Horse');
  var horse = new Horse();
  
  horse.name = req.body.name;
  horse.gender = req.body.gender;
  horse.bornDate = req.body.bornDate;
  horse.owner = req.body.owner;

  
  var status = false;
  
  //Zapisywanie do Mongo Sędziego
  horse.save(function(err, horse_Saved){
    if(err){
      console.log("Nie zapisane" + err);
      status = false;
    }else{
      status = true;
      console.log("Zapisano: " + horse.name );
      console.log("status" + status );
      
    }
    
    var data = {
      status: status
    }; 
    res.send(data);
  });
};
exports.showHorses = function(req, res){
    var Horse = mongoose.model('Horse');
  
    Horse.find({}, function (err, horse) {
      console.log("Znaleziono: " + horse);
      res.send(horse);
    });
  
};

exports.deleteHorse= function(req, res){
    var Horse = mongoose.model('Horse');
  
    console.log(req.params.id);
    var status = false;
  
    Horse.remove({_id: req.params.id},function(err){
        if (!err) {
            status = true;
            console.log("Usunięto");
        }
        else {
            status=false;
            console.log("Nie usunięto");
        }
      
        var data = {
            status: status
        }; 

        res.send(data);
    });
  
};

exports.showSingleHorse = function(req,res){
    var Horse = mongoose.model('Horse');
  
    Horse.find({ _id: req.params.id }, function (err, horse) {
      console.log("Znaleziono: " + horse);
      res.send(horse);
    });
};

exports.competitions = function(req, res){
    res.render('competitions', {
        isAuthenticated: req.isAuthenticated(),
        user: req.user
    });
};

exports.startCompetitionTransmision = function(req,res){
  var Competition = mongoose.model('Competition');
      Competition.update({ _id: req.body._id },  { transmision: true  }, function(err, comp){
         var status;
          if(err){
              console.log("Błąd w updatcie zawodów: " + err);
          }else {
              status=true;
          }
          var data = {
              status: status
          };
          res.send(data);
      });
};

exports.endCompetitionTransmision = function(req,res){
  var Competition = mongoose.model('Competition');
  var Category = mongoose.model('Category');
  
      Competition.update({ _id: req.body._id },  { transmision: false  }, function(err, comp){
         var status;
          if(err){
              console.log("Błąd w updatcie : " + err);
          }else {
              status=true;
          }
          cb();
      });
  var cb = function(){
    
    Competition.find({ _id: req.body._id }).populate('categories').exec(function (err, comp) {
        if (err) {
            console.log('Wystąpił błąd' + err);
        }else {

          if(comp[0].categories.length > 0 ) {
            for(var x= 0 ; x< comp[0].categories.length; x++){      
              Category.update({ _id:comp[0].categories[x]._id }, {transmision: false}, function(err, comp){
                  if(err){
                      console.log("Błąd w updatcie : " + err);
                  }else {
                  }
              });

            }
          }
          var data = {
              status: true
          };
          res.send(data);
        }

    }); 
    
  };

};

exports.addCompetition = function(req,res){
  
  var Competition = mongoose.model('Competition');
  var competition = new Competition();
  
  competition.name = req.body.name;
  competition.amount_of_referees = req.body.amount_of_referees;
  competition.range_of_rates = req.body.range_of_rates;
  competition.type_of_rates = req.body.type_of_rates;
  competition.category =[];

  var status = false;
  
  //Zapisywanie do Mongo Sędziego
  competition.save(function(err, competition_Saved){
    if(err){
      console.log("Nie zapisane" + err);
      status = false;
    }else{
      status = true;
      console.log("Zapisano: " + competition.name );
      console.log("status" + status );
      
    }
    
    var data = {
      status: status
    }; 
    res.send(data);
  });
};

exports.showCompetitions = function(req, res){
    var Competition = mongoose.model('Competition');
  
    Competition.find({}, function (err, competition) {
      //console.log("Znaleziono: " + competition);
      res.send(competition);
    });
  
};

exports.showSingleCompetition = function(req, res){
    var Competition = mongoose.model('Competition');
  
    Competition.find({ _id: req.params.id}).populate('startlist').exec(function (err, competition) {
        if (err) {
            console.log('Wystąpił błąd' + err);
        }else {
          
            res.send(competition);
        }

    });
};

exports.addToRefereeList = function(req,res){
  var Competition = mongoose.model('Competition');

  var competition;
  
  var status = false;
  Competition.find({_id: req.body._id }, function (err, data) {
      competition = data[0];
      var x = competition.referees.length;

      if(x >= competition.amount_of_referees){
        
        console.log("Juz jest maksymalna liczba sedziow");
        
          var data = {
              error: 'amount_of_referees',
              status: status
          };
          res.send(data);
      }else {
        
          competition.referees[x] = req.body.referee._id;
          checkRefereeList( req.body._id, req.body.referee._id);        
      }


  });
  var checkRefereeList = function( id ,idRef ){
      Competition.find({ _id: id ,referees: idRef }, function (err, data){
          if(err){
              console.log(err);
          }else{
            
              if(data.length === 0){
                  cb(competition);
              }else {
                
                  var amount = data[0].referees.length;
                
                  if(amount === 0){
                      cb(competition);
                  }else {
                    console.log("Sedzia juz znajduje się na liscie startowej");
                  }
                
              }
            
          }
        
      });
  };
  
  var cb = function(competition){
      Competition.update({ _id: req.body._id },  { referees: competition.referees  }, function(err, comp){
          if(err){
              console.log("Błąd w updatcie zawodów: " + err);
          }else {
              status=true;
          }
          var data = {
              status: status
          };
          res.send(data);
      });
  };

};

exports.showRefereeList = function(req, res){
    var Competition = mongoose.model('Competition');
  
  
    Competition.find({_id: req.params.id }).populate('referees').exec(function (err, referees) {
        if (err) {
            console.log('Wystąpił błąd' + err);
        }else {
            //console.log('Kon znaleziony to ' +  startlist[1].horse[0].name);
          var data = referees[0].referees;
            res.send(data);
        }

    });

};

exports.startCategoryTransmision = function(req,res){
   var Category = mongoose.model('Category');
  
      Category.update({ _id: req.body._id },  { transmision: true  }, function(err, category){
         var status; 
        if(err){
              console.log("Błąd w updatcie categorii: " + err);
          }else {
              status=true;
          }
          var data = {
              status: status
          };
          res.send(data);
      });
};

exports.addCategoryToCompetition = function(req, res){
  var Category = mongoose.model('Category');
  var category = new Category();
  
  console.dir(req.body);
  
  category.name = req.body.name;
  category._competition = req.body._competition._id;
  category._referees = req.body._referees._id;
  category._startlist = req.body._startlist._id;
  
  var status = false;
  
  //Zapisywanie do Mongo Sędziego
  category.save(function(err, category_Saved){
    if(err){
      console.log("Nie zapisane" + err);
      status = false;
    }else{
      status = true;
      console.log("Zapisano: " + category.name );
      console.log("status" + status );

    }
    
    var data = {
      status: status
    }; 
    res.send(data);
  });
  
};

exports.deleteCategory = function(req,res){
    var Category = mongoose.model('Category');
    var status = false;
  
    Category.remove({_id: req.params.id},function(err){
        if (!err) {
            status = true;
            console.log("Usunięto");
        }
        else {
            status=false;
            console.log("Nie usunięto");
        }
    });
  
    var data = {
        status: status
    }; 
    
    res.send(data);
};

exports.deleteCompetition = function(req,res){

    var Competition = mongoose.model('Competition');
    var status = false;
  
    Competition.remove({_id: req.params.id},function(err){
        if (!err) {
            status = true;
            console.log("Usunięto");
        }
        else {
            status=false;
            console.log("Nie usunięto");
        }
    });
  
    var data = {
        status: status
    }; 
    
    res.send(data);
};

exports.showCompetitionCategories = function(req, res){
    var Category = mongoose.model('Category');
  
    Category.find({_competition: req.params.id }).populate('_referees').exec(function (err, category) {
        if (err) {
            console.log('Wystąpił błąd' + err);
        }else {
            res.send(category);
        }

    });
  
};

exports.showSingleCategory = function(req, res){
    var Category = mongoose.model('Category');
  
    Category.find({_id: req.params.id }).populate('_referees').populate('_startlist').exec(function (err, category) {
        if (err) {
            console.log('Wystąpił błąd' + err);
        }else {
            res.send(category);
        }

    });
};

exports.showSingleCategoryHorses  = function(req, res){
    var Startlist = mongoose.model('Startlist');
  
    Startlist.find({category: req.params.id }).populate('horse').exec(function (err, startlist) {
        if (err) {
            console.log('Wystąpił błąd' + err);
        }else {
            res.send(startlist);
        }

    });
};

exports.addToStartList = function(req, res){
  var Startlist = mongoose.model('Startlist');
  var Horse = mongoose.model('Horse');
  var startlist = new Startlist();
  var actual_number=0;
  var status = false;
  
  
  Startlist.find({competition: req.body.competition._id},function (err, startlist) {

      actual_number = startlist.length +1;
      checkDatabase();
      
  });
  
  var checkDatabase = function() {
    
      Startlist.find({competition: req.body.competition._id,horse: req.body.horse._id }, function (err, startlist) {
        
          var x = startlist.length;
          console.log("Takich rekordów jest  : " + x);

          if(x === 0){

              cb(actual_number);

          }else {

            var data = {
                status: status
            };
            res.send(data);

          }
        
      });
  };
  
  var cb = function(actual_number) {
    
      startlist.number = actual_number;
      startlist.horse = req.body.horse._id;
      startlist.competition = req.body.competition._id;
      //startlist.gender = 'female';
      
      Horse.find({_id:req.body.horse._id }, function(err, data){
        if(err){
          console.log(err);
            
          }else {
            startlist.gender = data[0].gender;
            saveStarlist();
          }
        
      });
      var saveStarlist = function(){
          startlist.save(function(err, startlist_Saved){
              if(err){
                  console.log("Nie zapisane" + err);
                  status = false;
              }else{
                  status = true;
                  console.log("Zapisano: " + startlist.number );
                  console.log("status" + status );
              }

              var data = {
                  status: status
              }; 

              res.send(data);
          });     
      };

  };
};

exports.showStartList = function(req, res){
    var Startlist = mongoose.model('Startlist');

    Startlist.find({competition: req.params.id }).populate('horse').exec(function (err, startlist) {
        if (err) {
            console.log('Wystąpił błąd' + err);
        }else {
            //console.log('Kon znaleziony to ' +  startlist[1].horse[0].name);
            res.send(startlist);
        }

    });
};

exports.showStartListCategory = function(req, res){
    var Startlist = mongoose.model('Startlist');

    Startlist.find({competition: req.params.comp , category: req.params.cat }).populate('horse').exec(function (err, startlist) {
        if (err) {
            console.log('Wystąpił błąd' + err);
        }else {
            //console.log('Kon znaleziony to ' +  startlist[1].horse[0].name);
            res.send(startlist);
        }

    });
}

exports.updateRefereeRate = function(req,res){
  var Rate = mongoose.model('Rate');
  var Startlist = mongoose.model('Startlist');
  
  var overrall = (parseFloat(req.body.rate.motion) + parseFloat(req.body.rate.legs) + parseFloat(req.body.rate.log) + parseFloat(req.body.rate.head) + parseFloat(req.body.rate.type) )/5;
  
  var rate = new Rate();

  rate.referee = req.body.referee._id;
  rate.type = req.body.rate.type;
  rate.head_and_neck = req.body.rate.head;
  rate.log = req.body.rate.log ;
  rate.legs = req.body.rate.legs;
  rate.motion = req.body.rate.motion;
  rate.overall = overrall;
  //console.log("STARTLIST NuMNEr" +req.body.startlist.number );
  if( req.body.startlist.number.length > 0 ){
  
    Startlist.find({number: req.body.startlist.number , category: req.body.category._id}).populate('category').exec(function (err, startlist) {
        if (err) {
            console.log('Wystąpił błąd' + err);
        }else {

          if(startlist.length > 0){
              cb(startlist[0]._id);
          }
        }

    });
  }else{
    var data = {
          status: false,
          error: "Nie ma podanego numeru startowego"
    }; 
        res.send(data);
  }
  var cb = function(startlistid){
    
    Rate.find({startlist: startlistid , referee: req.body.referee._id}, function (err, rate) {
        
      if(rate.length > 0 ){
        updateRate(startlistid);
      }else{
        saveRate(startlistid);
      }
        
      });
  };

  var saveRate = function(startlistid){
        rate.startlist =startlistid;
        rate.save(function(err, rate_Saved){
          var status;
          if(err){
            console.log("Nie zapisane" + err);
            status = false;
          }else{
            status = true;
          }

          var data = {
            status: status,
            save: rate_Saved
          }; 
          res.send(data);
        });
  };

  var updateRate = function(startlistid){
    Rate.update({ referee: req.body.referee._id , startlist: startlistid},{ overall: overrall,  motion: req.body.rate.motion , legs: req.body.rate.legs , log: req.body.rate.log , head_and_neck: req.body.rate.head , type: req.body.rate.type  }, function(err, up){
          var status = false;
                if(err){
                console.log("Błąd w updatcie oceny ogolnej " + err);
            }else {
                console.log("Updatowało ocenę");
                status=true;
            }
            var data = {
                updated: up,
                status: status,
                overall: overrall
            };
            res.send(data);
        });
  };
  
};

exports.findRefereeRate = function(req,res){
  var Rate = mongoose.model('Rate');
  var Startlist = mongoose.model('Startlist');
  var rate;

    Startlist.find({number: req.params.startlist , category: req.params.cat }).populate('category').exec(function (err, startlist) {
          if (err) {
              console.log('Wystąpił błąd' + err);
          }else {

            if(startlist.length > 0){
                cb(startlist[0]._id);
            }
          }
    });

  var cb = function(startlistId){
    Rate.find({ referee: req.params.ref, startlist: startlistId },function (err, rate) {

        if(rate.length > 0 ){
          res.send(rate);
        }else{
                rate = new Rate();
                rate.referee = req.params.ref;
                rate.startlist = startlistId;
                rate.overall = 0;
                rate.motion = 0;
                rate.legs = 0;
                rate.log = 0;
                rate.head_and_neck = 0;
                rate.type = 0;
              
                rate.save(function(err, rate_Saved){
                    if(err){
                        console.log("Nie zapisane" + err);
                      
                    }else{
                        cb();
                    }
              });
        }

    });
  };


};

exports.setOverallRate = function(req,res){
  var Startlist = mongoose.model('Startlist');
  
  
  Startlist.findOne({ number: req.params.number, category: req.params.category}).populate('rate').exec(function (err, startlist) {
        if (err) {
            console.log('Wystąpił błąd' + err);
        }else {
            if(startlist.length > 0){              
                findStart(startlist._id);
            }else {
              
            }
          
        }

    });
  
  var findStart = function( idstart){
  Startlist.find({_id: idstart }).populate('rate').exec(function (err, startlist) {
        if (err) {
            console.log('Wystąpił błąd' + err);
        }else {
          if(startlist.length > 0){
              var note=0;
              var x = startlist[0].rate.length;
              
              for(var i=0; i< x; i++){
                console.log(startlist[0].rate[i].overall);
                note += startlist[0].rate[i].overall;
              }
              note = note/x;
              
              cb(note, idstart);
          }
        }

    });
  };
  
  var cb = function(note ,startId){
      Startlist.update({ _id: startId },{ overall: note  }, function(err, startlist){
           var status;
          if(err){
              console.log("Błąd w updatcie oceny ogolnej " + err);
          }else {
              
              status=true;
          }
          var data = {
              status: status,
              overall: note
          };
          res.send(data);
      });
  };
  
};