  //Moduł dla rejestracji
  var register = angular.module('register', []);

  register.controller('registrationCtrl', function($scope, $http){
  
      $scope.registerNew = function() {
          //console.log( "Email: "+ $scope.emailAdress + "Użytkownik: " + $scope.username);
          var data ={
              email: $scope.emailAdress,
              username: $scope.username,
              password: $scope.password,
              
          };

          var headers = {'Content-Type': 'application/json'};
        
        
          $http.post("/register/newUser", data, headers).success(function(data, status) {
              //console.log("udało się" + data.status);
              if(data.status === true){
                  $('.registerError').css("display", "none");
                  $('.registerAccept').css("display", "block");
              }else{
                  //alert('Nie udało się');
                  $('.registerError').css("display", "block");
                  $('.registerAccept').css("display", "none");
              }
          });
      };
  });

  //Moduł dla logowania
  var login = angular.module('login', []);

  login.controller('loginCtrl', function($scope, $http){


  });

  //Moduł dla Sędziego
  var referee = angular.module('referee', []);

  referee.controller('refereeCtrl' ,function($scope, $http){

   //Listing 
      var refereeListing = function(){
          $http.get('/referee/showReferees').success(function(data){
              console.log(data);
              $scope.referees = data;
          });   
      };
      refereeListing();
  
      //Dodawanie sędziego
      $scope.addNewReferee = function() {

          //console.log("Imie i nazwisko: "+ $scope.firstName + " " + $scope.lastName + "Email: "+ $scope.emailAdress + "Użytkownik: " + $scope.username);

          var data = {
                  firstName: $scope.firstName,
                  lastName: $scope.lastName,
                  email: $scope.emailAdress,
                  username: $scope.username,
                  password: $scope.password
          };

          var headers = {'Content-Type': 'application/json'};

          $http.post("/referee/addReferee", data, headers).success(function(data) {
              console.log(data);
              if(data.status === true){
                 // alert("Dodano sędziego");
                  $scope.firstName ='';
                  $scope.lastName = '';
                  $scope.emailAdress ='';
                  $scope.username='';
                  $scope.password='';
                  $('label').removeClass('onFocus');
                  refereeListing(); //odświerzenie listy sędziów
              }else{
                  alert("nie dodano sędziego");
              }
            });

      };
    
    
      $scope.removeReferee= function(id){
        //alert(id);
        $http.get('/referee/delete/'+ id).success(function(data){
          refereeListing();
        });
        
        
      };
      

  });
  
  //Konie
  var horse = angular.module('horse', []);

  horse.controller('horseCtrl', function($scope, $http){
      
    
    //dodanie Konia
      $scope.addNewHorse = function() {
        
        var horse_data = {
          name: $scope.name,
          gender: $scope.gender,
          bornDate: $scope.bornDate,
          owner: $scope.owner
        };
        console.log(horse_data);
        var headers = {'Content-Type': 'application/json'};
        
        $http.post('/horses/addHorse', horse_data, headers).success(function(data){
              console.log(data);
              if(data.status === true){
                  //alert("Dodano Konia");
                  $scope.name ='';
                  $scope.bornDate = '';
                  $scope.owner ='';
                  $('label').removeClass('onFocus');
                  horseListing(); //odświerzenie listy koni
              }else{
                  //alert("nie dodano konia");
              }
        });
      };
    
      //Listing Koni
      var horseListing = function(){
          $http.get('/horses/showHorses').success(function(data){
              //console.log(data);
              $scope.horses = data;
          });   
      };
      horseListing();
    
      //Usuwanie Konia
      $scope.removeHorse= function(id){
        //alert(id);
        $http.get('/horses/delete/'+ id).success(function(data){
          horseListing();
        });
        
        
      };
  });

  var competition = angular.module('competition', []);

  competition.controller('competitionCtrl', function($scope, $http){
    
    $scope.addCompetition = function() {
        var data = {
          name: $scope.name,
          amount_of_referees: $scope.amount_of_referees,
          range_of_rates: $scope.range_of_rates,
          type_of_rates: $scope.type_of_rates,
        };

        var headers = {'Content-Type': 'application/json'};
        $http.post('/competitions/addCompetition', data, headers).success(function(data){
                //console.log(data);
                if(data.status === true){
                    //alert("Dodano Zawody");
                    $scope.name ='';
                    $scope.amount_of_referees = '';
                    $scope.range_of_rates ='';
                    $('label').removeClass('onFocus');
                    competitionListing();
                }else{
                    //alert("nie dodano zawodow");
                }
          });
          
    };
    
    
      $scope.removeCompetition= function(id){
        //alert(id);
        $http.get('/competition/delete/'+ id).success(function(data){
          competitionListing();
        });
        
        
      };
    
      $scope.removeCategory = function(id,compId){
        //alert(id);
        $http.get('/category/delete/'+ id).success(function(data){
          competitionCategoriesListing(compId);
        });
        
        
      };
    
      var competitionListing = function(){
          $http.get('/competitions/showCompetitions').success(function(data){
              //console.log(data);
              $scope.competitions = data;
          });   
      };
      competitionListing();
    
      $scope.showSingleCompetition = function(id){
        $http.get('/competitions/showSingleCompetition/' + id ).success(function(data){
          $scope.singleCompetition = data[0];
          $('.form, .tableList, .addPanel').css('display', 'none');
          $('.single').css('display', 'block');
          $('.goBack, .createCategoryButton, .createStartlistButton,.createRefereelistButton').css('display', 'inline');


          console.log($scope.singleCompetition);
        });
        $scope.competitionStartlistListing(id);
        competitionCategoriesListing(id);
        showCompetitionRefereeListing(id);
        $scope.refereeSelection = [];
        $scope.horsesSelection = [];
        
        
      };
    

    
    
    //Checkboxy dla createCategoryToCompetition
        
      $scope.toggleRefereeSelection = function(idRefree) {
          var idx = $scope.refereeSelection.indexOf(idRefree);
                 
              // is currently selected
          if (idx > -1) {
              $scope.refereeSelection.splice(idx, 1);
          }

          // is newly selected
          else {
              $scope.refereeSelection.push(idRefree);
          }
        //  alert($scope.refereeSelection);
      };
    
      $scope.toggleHorsesSelection = function(idHorse) {
          var idx = $scope.horsesSelection.indexOf(idHorse);
                 
              // is currently selected
          if (idx > -1) {
              $scope.horsesSelection.splice(idx, 1);
          }

          // is newly selected
          else {
              $scope.horsesSelection.push(idHorse);
          }
         // alert($scope.horsesSelection);
      };
    
      $scope.createCategoryToCompetition = function(id){

    
          var data = {
            name: $scope.category_name,
            _competition: {
              _id: id
            },
            _referees: {
              _id: $scope.refereeSelection
            },
            _startlist: {
              _id: $scope.horsesSelection
            }
            
          };
          //console.log(data);
          var headers = {'Content-Type': 'application/json'};

          $http.post('/category/addToCompetitions', data, headers).success(function(data){
              //console.log(data);
              if(data.status === true){
                    //alert("Dodano Zawody");
                    $scope.category_name ='';
                    $('label').removeClass('onFocus');
                    //competitionListing();
                     competitionCategoriesListing(id);
              }else{
                  //alert("nie dodano zawodow");
              }
          });  
       
      };

      var competitionCategoriesListing = function(id){
          $http.get('/category/showCompetitionCategories/' + id ).success(function(data){
              //console.log(data);
              $scope.competitionCategories = data;
          });   
      };
    
      
      $scope.showSingleCategory = function(id){
          $http.get('/category/showSingleCategory/' + id ).success(function(data){
              //console.log(data);
              $scope.showCategory = data[0];
              $scope.showCategoryRefs = $scope.showCategory._referees;
              $scope.showCategoryStartLists = $scope.showCategory._startlist;
              
              //alert($scope.showCategory._referees[0].firstName);
          }); 
          $scope.showSingleCategoryHorses(id);
        
        $('.showCompCatSingle').css('display', 'block');
        $('.showCompCatSingle').addClass('tableList');
      };
    
      $scope.showSingleCategoryHorses = function(id){
          $http.get('/category/showSingleCategoryHorses/' + id ).success(function(data){

              $scope.showCategoryHorses = data;

          });
      };
    
      var horseListing = function(){
          $http.get('/horses/showHorses').success(function(data){
              //console.log(data);
              $scope.horses = data;
          });   
      };
    horseListing();
    
    $scope.addToStartList = function(comp_id) {
       // alert($scope.horse_id);
        var data = {
          number: 1,
          horse: {
            _id: $scope.horse_id,
            //_id: $scope.horse_id.name

          },
          competition: {
            _id: comp_id
          }         
        };

        var headers = {'Content-Type': 'application/json'};
        $http.post('/startList/addToStartList', data, headers).success(function(data){
                //console.log(data);
                if(data.status === true){
                  //console.log("dodano do listy startowej");
                  $scope.competitionStartlistListing(comp_id);
                }else{
                    alert("Ten koń został wcześniej zapisany na zawody");
                }
          
          });
          
    };
    
      $scope.competitionStartlistListing = function(id){
          $http.get('/startList/showStartList/' + id ).success(function(data){
             // console.log("Pobrano" + data);
              $scope.competitionStartlists = data;
              $scope.checkMale = function($competitionStartlists) {
                      return $competitionStartlists.horse[0].gender === 'ogier';
              };
              
              $scope.checkFemale = function($competitionStartlists) {
                      return $competitionStartlists.horse[0].gender === 'klacz';
              };
              $scope.checkGender= $scope.checkFemale;
              $scope.setMaleCat = function(){
                  $('.setCategoryMale').addClass('activeCat');
                  $('.setCategoryFemale').removeClass('activeCat');
                  $scope.checkGender= $scope.checkMale;
              };
              $scope.setFemaleCat = function(){
                  $('.setCategoryFemale').addClass('activeCat');
                  $('.setCategoryMale').removeClass('activeCat');
                  $scope.checkGender= $scope.checkFemale;  
              };
              
          });   
        
          

      };
    
    
      var refereeListing = function(){
          $http.get('/referee/showReferees').success(function(data){         
              $scope.referees = data;
          });   
      };
      refereeListing();
      
      
    $scope.addToRefereeList = function(id) {

        var data = {
            _id: id,
            referee: {
                _id: $scope.referee_id
            }   
        };

        var headers = {'Content-Type': 'application/json'};
        $http.post('/competition/addToRefereeList', data, headers).success(function(data){
                //console.log(data);
                if(data.status === true){
                    //alert("Dodano Zawody");
                  //console.log("dodano sedziego do listy startowej");
                  showCompetitionRefereeListing(id);
                }else{
                    //alert("nie dodano Sedziego do listy");
                }
          
          });
          
    };
    
     var showCompetitionRefereeListing = function(id){
          $http.get('/competition/showRefereeList/' + id ).success(function(data){
             //alert(data);
              $scope.competitionRefereeLists = data;
          });  
     };
    
     $scope.startCompetitionTransmision = function(id){
        var data = {
            _id: id,
        };

        var headers = {'Content-Type': 'application/json'};
        $http.post('/startCompetitionTransmision', data, headers).success(function(data){
                //console.log(data);
                if(data.status === true){
                  //console.log("Zmieniono status transmisji");

                }else{
                    //alert("nie zmieniono statusu transmisji");
                }
          $('.transmisionAdmin,.groups').css('display', 'block');
              $scope.showSingleCompetition(id);
              
          });
    };
     $scope.endCompetitionTransmision = function(id){
        var data = {
            _id: id,
        };

        var headers = {'Content-Type': 'application/json'};
        $http.post('/endCompetitionTransmision', data, headers).success(function(data){
                console.log(data);
                if(data.status === true){
                  //console.log("Zmieniono status transmisji");

                }else{
                    //alert("nie zmieniono statusu transmisji");
                }
              
          });
    };
     
     $scope.startCategoryTransmision = function(id){
        var data = {
            _id: id,
        };

        var headers = {'Content-Type': 'application/json'};
        $http.post('/startCategoryTransmision', data, headers).success(function(data){
                //console.log(data);
                if(data.status === true){
                 // console.log("Zmieniono status transmisji");

                }else{
                    //alert("nie zmieniono statusu transmisji");
                }

              
          });
       
       $scope.showSingleCategory(id);
       $('.transmisionOnLive').css('display','block');
    };
    
     
     
     
     //Transmisja Admin
     

      $( document ).ready(function() {
        
        
        
        //Admin - ref
        var onLive = io('https://' + location.host + '/onLive');
        
        
        onLive.on('message', function (data) {
            //console.log(data);

       

            var container = $('#msgFromRef');
            //container.empty();
            var table = $('<table class="rate "></table>');
           // table.empty();
            var tr = $('<tr class="' +data.referee.firstName + data.referee.lastName +  '"></tr>');
            tr.empty();
            container.append(table);
            table.append(tr);
          
            tr.append('<td> Sędzia :<b> ' + data.referee.firstName + ' ' + data.referee.lastName + '</b></td>');
            tr.append('<td>Nr startowy: <b>' + data.startlist.number + '</b></td>');
            tr.append('<td>typ: <b>' + data.rate.type + '</b></td>');
            table.append(tr);
            tr.append('<td>głowa i szyja: <b>' + data.rate.head + '</b></td>');
            tr.append('<td>tułów: <b>' + data.rate.log + '</b></td>');
            tr.append('<td>nogi: <b>' + data.rate.legs + '</b></td>');
            table.append(tr);
            tr.append('<td>ruch: <b>' + data.rate.motion + '</b></td>');
            tr.append('<td>ogólna: <b>' + data.rate.overall + '</b></td>');
         // alert(data.category._id);
          
          $scope.refreshMsgFromRef = function(){
            container.empty();
          };
          
          
          
          
          setInterval(function(){
            setOverall(data.startlist.number, data.category._id , data.competition._id);
          },5000);
          
          
        var setOverall = function( start, cat, comp ) {
                var headers = {'Content-Type': 'application/json'};
                     $.ajax({
                          url:'/setOverallRate/' + start + '/' + cat,
                          header: headers,
                          method:'get',
                          success: function(d){

                            //Odświeżenie overalla na stronie panelu administratora
                            $scope.showSingleCategory(cat);
                            $scope.competitionStartlistListing(comp);  
                          }        
                        
                  });
          
        };


        });
        
        var liveTable = io('https://' + location.host + '/liveTable');
        
        
        $scope.sendLiveTableToUser = function(id){
          //alert('udało się');
          liveTable.emit('message', id);
          console.log('Wysłano do userow' + id);
        };
        
        
        var sendLive = io('https://' + location.host + '/sendLive');
        
        //Wysyłanie danych do Sędziego
        $('#sendToRefs').on('click', function(){
         // alert($scope.showCategory.name);
            

          sendLive.emit('message', $('#startListNumber').val());  
        });
        setInterval(function(){
          sendLive.emit('message', $('#startListNumber').val());  
        },2000);
        
        
        //Admin - user
        
        var liveT = io('https://' + location.host + '/liveT');
        var infoChat = io('https://' + location.host + '/infoChat');
      
        liveT.on('message', function (data) {
			console.log(data);
		});
        
       var dataSendToUser = '';
        $("#sendToWatchers").on('click', function(){
          var data = $("#msgToWatcher").val();
          dataSendToUser = '<li>' + data + '</li>' + dataSendToUser;
          console.log(dataSendToUser);
          liveT.emit('message', dataSendToUser);
        });
            
        setInterval(function(){
          liveT.emit('message', dataSendToUser);  
        },10000);
        
        $("#sendInfoToRefs").on('click', function(){
          var data = $("#msgInfoToRefs").val();
          infoChat.emit('message', data);
        });
        
        
      });
     
    
  });


  var refereePanel = angular.module('refereePanel', []);

  refereePanel.controller('refPanelCtrl', function($scope, $http){
  
      var RefereePanelLiveData = function(){
            $http.get('/refereePanelFindLive').success(function(data){
               //alert(data);
                $scope.singleRefereeLiveCompetitions = data.refereeLiveCompetitions;
                $scope.singleRefereeLiveCategories = data.refereeLiveCategories;
                $scope.refereeInfo = data.referee;
            }); 
      };
      RefereePanelLiveData();
    
      var RefereePanelData = function(){
            $http.get('/refereePanelFind').success(function(data){
               //alert(data);
                $scope.singleRefereeCompetitions = data.refereeCompetitions;
                $scope.singleRefereeCategories = data.refereeCategories;
            }); 
      };
      RefereePanelData();
    
      $scope.showLiveRefCategory = function(id){
        
            $http.get('/showSingleLiveCategory/' + id).success(function(data){
               //alert(data[0].name);
              
              $scope.LiveRefCategory = data[0];
                //console.log("data z liveCategoryRef" + data);
              $('.mainRefPanel ').css('display', 'none');
              $('.judgePanel,.goBack').css('display', 'block');
              
              findRates();   
            }); 
       
      };
    
    //Socket dla Sedziego
      var onLive = io('https://' + location.host + '/onLive');
      var sendLive= io('https://' + location.host + '/sendLive');
      var infoChat = io('https://' + location.host + '/infoChat');
    
      var horseNumber	= document.getElementById("horseNumber");
      var infoFromAdmin	= document.getElementById("infoFromAdmin");
    
      sendLive.on('message', function (data) {
            //console.log(data.startlist. );
          var number =  data;
        if( $scope.horseNumber != data){

        $scope.$apply(function(){
          $scope.horseNumber = data;
        });
        findRates();
        }
        
          
        //$scope.ocena = data;
      });
    
       
    
    
      infoChat.on('message', function (data) {
          $("#infoFromAdmin").css('display','block');
        
          console.log(data);
          infoFromAdmin.textContent = data;
        
          setTimeout(function(){
            $("#infoFromAdmin").css('display','none');
          },15000);
        //$scope.ocena = data;
      });
    
      $( document ).ready(function() {
          $('#send').on('click', function(){
              
              onLive.emit('message', $('#m').val());  
          });
      });
    
    
    //Wysyłanie oceny
        var findRates = function(){

          var headers = {'Content-Type': 'application/json'};
            $.ajax({
                url:'/findRefereeRate/' + $scope.refereeInfo.id + '/' + $scope.horseNumber + '/' + $scope.LiveRefCategory._id ,
                header: headers,
                method:'get',
                success: function(d){
                  if(d.length>0){
                      console.log('DATA LOG' + d[0].log);
                      $scope.$apply(function(){
                          $scope.rate_log = d[0].log;
                          $scope.rate_head = d[0].head_and_neck;
                          $scope.rate_motion = d[0].motion;
                          $scope.rate_legs = d[0].legs;
                          $scope.rate_type = d[0].type;

                      });

                  //    setTimeout(function(){
                 //           sendingRates();
                   //   },1000);  
                    
                  }else{              

                 //     setTimeout(function(){
                       //     sendingRates();
                    //  },1000);
                  }
                }            
            });
        };
        
    var sendingRates = function(){
        setInterval(function(){ 
          var overrall = ($scope.rate_type + $scope.rate_head + $scope.rate_log + $scope.rate_legs + $scope.rate_motion )/5;
          var data = {
              competition:{
                _id: $scope.LiveRefCategory._competition[0]._id
              },
              startlist:{
                number: $scope.horseNumber
              },
              referee: {
                _id: $scope.refereeInfo.id,
                username: $scope.refereeInfo.username,
                firstName:$scope.refereeInfo.firstName,
                lastName:$scope.refereeInfo.lastName,
              },
              rate: {
                type: $scope.rate_type,
                head: $scope.rate_head,
                log: $scope.rate_log,
                legs: $scope.rate_legs,
                motion: $scope.rate_motion,
                overall: overrall
              },
              category:{
                _id: $scope.LiveRefCategory._id
              }
          };
              var headers = {'Content-Type': 'application/json'};
                          $.ajax({
                              url:'/updateRefereeRate/',
                              header: headers,
                              method:'post',
                              data:data,
                              success: function(d){
                                console.log('updatetowalo');
                              }


                      });
        }, 5000); //5sek
  };
    sendingRates();
    var sentToAdmin = function(){
          var overrall = ($scope.rate_type + $scope.rate_head + $scope.rate_log + $scope.rate_legs + $scope.rate_motion )/5;
          var data = {
              competition:{
                _id: $scope.LiveRefCategory._competition[0]._id
              },
              startlist:{
                number: $scope.horseNumber
              },
              referee: {
                _id: $scope.refereeInfo.id,
                username: $scope.refereeInfo.username,
                firstName:$scope.refereeInfo.firstName,
                lastName:$scope.refereeInfo.lastName,
              },
              rate: {
                type: $scope.rate_type,
                head: $scope.rate_head,
                log: $scope.rate_log,
                legs: $scope.rate_legs,
                motion: $scope.rate_motion,
                overall: overrall
              },
              category:{
                _id: $scope.LiveRefCategory._id
              }
          }; 
          onLive.emit('message', data);
          
    };
    
    setTimeout(function(){
      setInterval(function(){
        sentToAdmin();
      },20000);
    },10000);
    
    
  });
  
  
  
      
///Jquery


    $( document ).ready(function() {  

      
      
          //Slajder
      var slajd = 2;
      var zmianaSlajdu = function(time){
      
          var url = 'url(../img/kon' + slajd + '.jpg)';
          setTimeout(function(){
              //console.log("slajd" + slajd);
              $('body').css('background', url);

              slajd++;

              if(slajd === 5){
                  slajd =2;
                  zmianaSlajdu(time);
              }else {
                    zmianaSlajdu(time);  
              }
          },time);
      };
    
      zmianaSlajdu(10000);  
      
      
      //Panel dodawania znikanie/pojawianie sie
      $('.addPanel').on('click', function(){
        $('.form.competitionAdd').toggle();
      });
      
      $('.goBack').on('click', function(){
         $('.competitionAdd, .tableList, .addPanel ,.mainRefPanel').css('display', 'block');
         $('.single, .category, .createCategoryButton, .createStartlistButton, .startList,.createRefereelistButton,.refereeList ,.judgePanel, .showCompCatSingle, .groups ,.transmisionOnLive').css('display','none');
        $(this).css('display', 'none');
      });
      
      
      $('.showSingleCompCategory').on('click' , function(){
        
        $('.showCompCatSingle').css('display', 'block');
      });
      
      $('.createCategoryButton').on('click', function(){
        $('.category').toggle();
      });
      $('.createStartlistButton').on('click', function(){
        $('.startList').toggle();
      });
      $('.createRefereelistButton').on('click', function(){
        $('.refereeList').toggle();
      });
      
      
      $('.fa-bars').on('click',function(){
          $('.myMenu .menu li a').toggle();
      });
      
      $('.closeAddPanel').on('click',function(){
        $('.refereeList, .startList').css('display', 'none');
      });
      
      
      //Formularze Animacja
      $(".firstName").on( 'focus', function(){
        $('label[for="firstName"]').addClass('onFocus');
      });
      
      $(".firstName").on( 'blur', function(){
        if($(this).val() === '') {
          $('label[for="firstName"]').removeClass('onFocus');
        }
      });
      
      $(".lastName").on( 'focus', function(){
        $('label[for="lastName"]').addClass('onFocus');
      });
      
      $(".lastName").on( 'blur', function(){
        if($(this).val() === '') {
          $('label[for="lastName"]').removeClass('onFocus');
        }
      });
      
      $(".email").on( 'focus', function(){
        $('label[for="email"]').addClass('onFocus');
      });
      
      $(".email").on( 'blur', function(){
        if($(this).val() === '') {
          $('label[for="email"]').removeClass('onFocus');
        }
      });
      
      $(".username").on( 'focus', function(){
        $('label[for="username"]').addClass('onFocus');
      });
      
      $(".username").on( 'blur', function(){
        if($(this).val() === '') {
          $('label[for="username"]').removeClass('onFocus');
        }
      });
      
      $(".pass").on( 'focus', function(){
        $('label[for="pass"]').addClass('onFocus');
      });
      
      $(".pass").on( 'blur', function(){
        if($(this).val() === '') {
          $('label[for="pass"]').removeClass('onFocus');
        }
      });
      
      $(".name").on( 'focus', function(){
        $('label[for="name"]').addClass('onFocus');
      });
      
      $(".name").on( 'blur', function(){
        if($(this).val() === '') {
          $('label[for="name"]').removeClass('onFocus');
        }
      });
      
      $(".bornDate").on( 'focus', function(){
        $('label[for="bornDate"]').addClass('onFocus');
      });
      
      $(".bornDate").on( 'blur', function(){
        if($(this).val() === '') {
          $('label[for="bornDate"]').removeClass('onFocus');
        }
      });
      
      $(".owner").on( 'focus', function(){
        $('label[for="owner"]').addClass('onFocus');
      });
      
      $(".owner").on( 'blur', function(){
        if($(this).val() === '') {
          $('label[for="owner"]').removeClass('onFocus');
        }
      });
      
      $(".amountOfReferees").on( 'focus', function(){
        $('label[for="amount_of_referees"]').addClass('onFocus');
      });
      
      $(".amountOfReferees").on( 'blur', function(){
        if($(this).val() === '') {
          $('label[for="amount_of_referees"]').removeClass('onFocus');
        }
      });
      
      $(".rangeOfRates").on( 'focus', function(){
        $('label[for="range_of_rates"]').addClass('onFocus');
      });
      
      $(".rangeOfRates").on( 'blur', function(){
        if($(this).val() === '') {
          $('label[for="range_of_rates"]').removeClass('onFocus');
        }
      });
      
      $(".categoryName").on( 'focus', function(){
        $('label[for="category_name"]').addClass('onFocus');
      });
      
      $(".categoryName").on( 'blur', function(){
        if($(this).val() === '') {
          $('label[for="category_name"]').removeClass('onFocus');
        }
      });
    });