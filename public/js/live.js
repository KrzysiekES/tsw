  var app = angular.module('onlive', []);

  app.controller('liveCtrl', function($scope,$http){
    
      $scope.showSingleCompetition = function(id){
          $http.get('/competitions/showSingleCompetition/' + id ).success(function(data){
              $scope.singleCompetition = data[0];
              
              //console.log($scope.singleCompetition);
          });
        
          $http.get('/startList/showStartList/' + id ).success(function(data){
             
              $scope.competitionStartlists= data;
             
              $scope.checkMale = function($competitionStartlists) {
                      return $competitionStartlists.horse[0].gender === 'ogier';
              };
              
              $scope.checkFemale = function($competitionStartlists) {
                      return $competitionStartlists.horse[0].gender === 'klacz';
              };
            
            
          });
        
        
      
          $http.get('/category/showCompetitionCategories/' + id ).success(function(data){
              //console.log(data);
              $scope.competitionCategories = data;
          });     
        

      };
    
      $scope.showStartListCategory = function(compId, catID){
          $http.get('/startList/showStartListCategory/' + compId +'/' + catID ).success(function(data){
              //console.log(data);
            $('.liveCategoryTable').css('display','block');
              $scope.startListCategories = data;
          });     
        
      };
    

      var liveTable = io('https://' + location.host + '/liveTable');
      
      liveTable.on('message', function (data) {
         // alert(data);
          console.log(data);
          $('.liveCategoryTable').css('display','none');
          $scope.showSingleCompetition(data); 
          
      }); 
  });