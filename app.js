/*jshint node: true */
var express = require('express');
var app = express();

var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');
var mongoose = require('mongoose');
var serveStatic = require("serve-static");
var less = require('less-middleware');

app.use(serveStatic("public"));

var routes = require('./routes'); //pobranie folderu routes

// Passport.js
var passport = require('passport');
var passportLocal = require('passport-local');
var passportHttp = require('passport-http');
var passwordHash = require('password-hash');

// Serwer HTTPS
// openssl req -x509 -nodes -days 365 -newkey rsa:1024 -out my.crt -keyout my.key
var fs = require('fs');
var https = require('https');
var server = https.createServer({
    key: fs.readFileSync('./ssl/my.key'),
    cert: fs.readFileSync('./ssl/my.crt')
}, app);

// parametry aplikacji
var port = process.env.PORT || 3000;
var secret = process.env.APP_SECRET || '$sekretny $sekret';
var configDB = require('./config/database');
mongoose.connect(configDB.url);


//WebSocket
var socketio = require("socket.io");
var io = socketio.listen(server);

io.on('connection', function(ws){
  
    io.send('Witaj na serverze ws')
});

var onLive = io
    .of('/onLive')
    .on('connection', function (socket) {
        console.log('Uruchomiłem kanał "/onLive"');
        socket.on('message', function (data) {
        	socket.broadcast.emit('message',  data);
        });
    });
var sendLive = io
    .of('/sendLive')
    .on('connection', function (socket) {
        console.log('Uruchomiłem kanał "/sendLive"');
        socket.on('message', function (data) {
        	socket.broadcast.emit('message',  data);
        });
    });

var liveT = io
    .of('/liveT')
    .on('connection', function (socket) {
        console.log('Uruchomiłem kanał "/liveT"');
        socket.on('message', function (data) {
        	socket.broadcast.emit('message',  data);
        });
    });

var infoChat = io
    .of('/infoChat')
    .on('connection', function (socket) {
        console.log('Uruchomiłem kanał "/infoChat"');
        socket.on('message', function (data) {
        	socket.broadcast.emit('message',  data);
        });
    });


var liveTable = io
    .of('/liveTable')
    .on('connection', function (socket) {
        console.log('Uruchomiłem kanał "/liveTable"');
        socket.on('message', function (data) {
        	socket.broadcast.emit('message',  data);
        });
    });


//Połączenie z baza danych
var db = mongoose.connection;
db.on('open', function () {
    console.log('Połączono z MongoDB!');
});

db.on('error', console.error.bind(console, 'MongoDb Error: '));

// Model Mongoose reprezentujący uzytkownika i sędziego
var User = require('./models/user');
var Horse = require('./models/horse');
var Competition = require('./models/competition');
var Category = require('./models/category');
var Startlist = require('./models/startlist');
var Rate = require('./models/rate');

//Ustalenie szablonów na EJS
app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


app.use(cookieParser());
app.use(expressSession({
    secret: secret,
    resave: false,
    saveUninitialized: false
}));

// Używamy Passport.js
app.use(passport.initialize());
app.use(passport.session());

// Konfiguracja Passport.js
var validateUser = function (username, password, done){
    User.findOne({username: username}, function (err, user) {
        if (err) {
            done(err);
        }
        if (user) {
            console.log("HASŁO USERA: " + user.password);
            console.log("podane hasło: " + passwordHash.generate(password));
            console.log(passwordHash.verify(password, user.password));
            
            if(passwordHash.verify(password, user.password) === true){
                done(null, user);
            } else {
                done(null, null);
            }
        } else {
            done(null, null);
        }
    });
};


passport.use(new passportLocal.Strategy(validateUser));
passport.use(new passportHttp.BasicStrategy(validateUser));
passport.serializeUser(function (user, done) {
    done(null, user.id);
});


passport.deserializeUser(function (id, done) {
    User.findOne({"_id": id}, function (err, user) {
        if (err) {
            done(err);
        }
        if (user) {
            done(null, {
                id: user._id,
                username: user.username,
                firstName:user.firstName,
                lastName:user.lastName,
                password: user.password,
                type: user.type
            });
        } else {
            done({
                msg: 'Nieznany ID'
            });
        }
    });
});

// Routing aplikacji

//przekierowanie na strone głowna
app.get('/', routes.index);
app.get('/competitionlist', routes.competitionlist);

// rejestracja
app.get('/register', routes.register);
//Nowy admin
app.post('/register/newUser',routes.registerNew);

//Przekierowanie do strony login
app.get('/login', routes.redirectLogin);

//
app.post('/login', passport.authenticate('local', 
    { 
        successRedirect: '/',
        failureRedirect: '/login',
        failureFlash: true 
    })
);

//wylogowanie
app.get('/logout', routes.logout);

/*app.get('/api/users', passport.authenticate('basic',{
    session: false
}), routes.users);*/

//Na zywo
app.get('/live', routes.live);

//sędziowie
app.get('/refereePanel', routes.refereePanel);
app.get('/refereePanelFindLive', routes.refereePanelFindLive);
app.get('/showSingleLiveCategory/:id', routes.showSingleLiveCategory);
app.get('/refereePanelFind', routes.refereePanelFind);
app.get('/referee', routes.referee);
app.get('/referee/showReferees', routes.showReferees);
app.post('/referee/addReferee', routes.addReferee);        
app.get('/referee/delete/:id', routes.deleteReferee );

//Konie
app.get('/horses', routes.horses);
app.post('/horses/addHorse', routes.addHorse);
app.get('/horses/showHorses', routes.showHorses);
app.get('/horses/delete/:id', routes.deleteHorse);
app.get('/horses/showSingle/:id', routes.showSingleHorse);

//Zawody
app.get('/adminTransmisions', routes.adminTransmisions);
app.get('/competitions', routes.competitions);
app.post('/startCompetitionTransmision', routes.startCompetitionTransmision);
app.post('/endCompetitionTransmision', routes.endCompetitionTransmision);
app.post('/competitions/addCompetition', routes.addCompetition);
app.get('/competitions/showCompetitions', routes.showCompetitions);
app.get('/competitions/showSingleCompetition/:id', routes.showSingleCompetition);
app.post('/competition/addToRefereeList', routes.addToRefereeList);
app.get('/competition/showRefereeList/:id', routes.showRefereeList);
app.get('/competition/delete/:id', routes.deleteCompetition);

//Kategorie
app.post('/category/addToCompetitions', routes.addCategoryToCompetition);
app.get('/category/delete/:id', routes.deleteCategory);
app.get('/category/showCompetitionCategories/:id', routes.showCompetitionCategories);
app.get('/category/showSingleCategory/:id', routes.showSingleCategory);
app.get('/category/showSingleCategoryHorses/:id', routes.showSingleCategoryHorses );
app.post('/startCategoryTransmision', routes.startCategoryTransmision);

//Zapis Oceny
app.post('/updateRefereeRate', routes.updateRefereeRate);
app.get('/findRefereeRate/:ref/:startlist/:cat', routes.findRefereeRate);

//Lista startowa
app.post('/startList/addToStartList', routes.addToStartList);
app.get('/startList/showStartList/:id', routes.showStartList);
app.get('/startList/showStartListCategory/:comp/:cat', routes.showStartListCategory);

app.get('/setOverallRate/:number/:category', routes.setOverallRate);

// Uruchamiamy serwer HTTPS
server.listen(port, function () {
    console.log('https://localhost:' + port);
});